### IMPORTANT NOTE

this project will have only invisible rewrites until the 
hackathon is over. NO FEATURES WILL BE ADDED. I will not deploy
this version until the hackathon is over.

Currently, the project is inoperable - some client side changes broke
the contract that I need to update.

UPDATES:

1. Convert Queue worker to a rust script
2. POSSIBLY rewrite this server in actix web


### HOW TO RUN

create a .env file with these vars:
WIT_CLIENT_TOKEN=4FOQFJBTIDNBYE4SAHMADHABNGMLJI4L
GOOGLE_APPLICATION_CREDENTIALS=./gcp_key.json (so we can upload to GCP)
BUCKET_NAME=*** (GCP bucket name)
BASE_PATH=(path you're running this from. this is for emails to be able to send href links)
EMAIL_USERNAME=(email)
EMAIL_PASSWORD=(password)

activate env and run "rq worker --with-scheduler" and "uvicorn run:app"
which will start the redis queue and server respectively. navigating to <URL>/ will start the app.

You must have FFMPEG installed on your machine. 
