from email.mime.text import MIMEText
from google.cloud import storage
import smtplib
import stopit
import asyncio
import httpx
import os


def find_bucket():
    storage_client = storage.Client()
    return storage_client.bucket(os.environ['BUCKET_NAME'])


async def post(url, data, headers):
    async with httpx.AsyncClient(timeout=None) as client:
        res = await client.post(url, headers=headers, data=data)
    return res.json()


def upload_to_gcs(path, source_file, dest_name):
    bucket = find_bucket()
    blob = bucket.blob(f'{path}/{dest_name}')
    if isinstance(source_file, str):
        blob.upload_from_filename(source_file)
    else:
        blob.upload_from_file(source_file)


def delete_from_gcs(path, file_name):
    bucket = find_bucket()
    blob = bucket.blob(f'{path}/{file_name}')
    blob.delete()


def download_from_gcs(path, file_name, download_to_dir):
    bucket = find_bucket()
    blob = bucket.blob(f'{path}/{file_name}')
    blob.download_to_filename(f'{download_to_dir}/{file_name}')


def get_real_path():
    return os.path.dirname(os.path.realpath(__file__))

def delete_local_file(file_path):
    os.remove(file_path)
 
def send_email(to, header, body):
    msg = MIMEText(body, 'html')
    msg['From'] = os.environ["EMAIL_USERNAME"]
    msg['To'] = to
    msg['Subject'] = header

    with smtplib.SMTP('smtp.gmail.com', 587, timeout=1000) as server:
        server.ehlo()
        server.starttls()
        server.login(os.environ["EMAIL_USERNAME"],
                     os.environ["EMAIL_PASSWORD"])
        server.sendmail(os.environ["EMAIL_USERNAME"], [to], msg.as_string())
