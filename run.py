from dotenv import load_dotenv
from fastapi import FastAPI
from redis import Redis
from rq import Queue
import os

load_dotenv()
app = FastAPI()
q = Queue(connection=Redis())

import main