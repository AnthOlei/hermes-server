from operator import itemgetter
from itertools import groupby
 

def merge_overlapping_commands(commands):
    if len(commands) == 1:
        commands[0]["from"] = commands[0]["from"]
        commands[0]["to"] = commands[0]["to"]
        return commands

    fullset = set()
    for command in commands:
        newset = set(range(command["from"], command["to"]) + 1)
        fullset.update(newset)

    ranges = []

    for k,g in groupby(enumerate(fullset),lambda x:x[0]-x[1]):
        group = (map(itemgetter(1),g))
        group = list(map(int,group))
        ranges.append((group[0],group[-1]))

    new_vals = []
    for second_range in ranges[::-1]:
        new_vals.append({
            "from": second_range[0],
            "to": second_range[1]
        })
    return new_vals

def gen_mute(from_hms, to_hms):
    return f"volume=enable='between(t,{from_hms},{to_hms})':volume=0,"

def gen_single_ff(from_s, to_s, input, output, speed=2.0):
    command = f'ffmpeg -nostats -hide_banner -loglevel warning -i {input} -filter_complex "[0:v]trim=0:{from_s},setpts=PTS-STARTPTS[v1];[0:v]trim={from_s}:{to_s},setpts={1/speed}*(PTS-STARTPTS)[v2];[0:v]trim={to_s},setpts=PTS-STARTPTS[v3];[0:a]atrim=0:{from_s},asetpts=PTS-STARTPTS[a1];[0:a]atrim={from_s}:{to_s},asetpts=PTS-STARTPTS,atempo={speed}[a2];[0:a]atrim={to_s},asetpts=PTS-STARTPTS[a3];[v1][a1][v2][a2][v3][a3]concat=n=3:v=1:a=1" {output}'
    return command

def inverse_ranges(commands, endtime):
    new_commands = []
    last_end = 0

    for command in commands:
        if command["from"] == last_end: #skip so we don't have an n to n range
            last_end = command["to"]
            continue
        new_commands.append({
            "from": last_end,
            "to": command["from"]
        })
        last_end = command["to"]
    new_commands.append({
        "from": float(last_end),
        "to": float(endtime)
    })
    return new_commands

def order_commands(commands):
    commands.sort(key=lambda x: x["from"])
    return commands

def adjust_to_cuts(cuts, adjust):
    '''
    inputs must be ordered
    '''
    total_adjust = 0
    current_cut_index = 0
    current_adjust_index = 0
    new_adjust = []
    while current_adjust_index < len(adjust):
        #convert to floats here because for some reason they're getting in as STR, but 
        #This submission is due tomorrow so I can't find out why
        while len(cuts) > current_cut_index and float(cuts[current_cut_index]["to"]) < float(adjust[current_adjust_index]["from"]):
            total_adjust += float(cuts[current_cut_index]["to"]) - float(cuts[current_cut_index]["from"])
            current_cut_index += 1
        new_adjust.append({
            "from": adjust[current_adjust_index]["from"] - total_adjust,
            "to": adjust[current_adjust_index]["to"] - total_adjust
        })
        current_adjust_index += 1 
    return new_adjust
