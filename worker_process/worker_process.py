from helpers import download_from_gcs, delete_from_gcs, get_real_path, upload_to_gcs, send_email
from worker_process.worker_services import process_ffmpeg
from dotenv import load_dotenv
from json import loads, dumps
import stopit
import os


def process_file(serialized):
    load_dotenv()
    items = loads(serialized)
    download_from_gcs("raw", items["filename"],
                      f'{get_real_path()}/processing_files')
    delete_from_gcs("raw", items["filename"])
    try:
        path = process_ffmpeg(
            items["filename"], items["commands"], items["endtime"])
        name = path.split('/')[-1]
        upload_to_gcs('processed', path, name)
        send_conf_email(items["email"], name)
        os.system(f'rm {get_real_path()}/processing_files/*')
    except:
        send_failure_email(items["email"])
        raise

def send_failure_email(email):
    body = """
        <html>
     <head></head>
        <body>
            <p>
            Unfortunately, there was an error editing your file. We're pouring over the logs
            to make sure it doesn't happen again. sorry for the inconvienence.
            </p>
        </body> 
    </html>
    """

    header = "Your Hermes video failed editing :("

    send_email(email, header, body)


def send_conf_email(email: str, filename: str):
    body = f"""
    <html>
     <head></head>
        <body>
            <p>
            Your edited file is ready! Click here to download <a href="{os.environ["BASE_PATH"]}/download/{filename}">link</a>.

            IMPORTANT: This link is good for ONE download. after that, the file will be deleted. 
            </p>
        </body> 
    </html>
    """

    header = "Your Hermes video editor file is ready!"

    send_email(email, header, body)
