from helpers import get_real_path, delete_local_file
from worker_process.helpers import merge_overlapping_commands, gen_mute, gen_single_ff, inverse_ranges, order_commands, adjust_to_cuts
from subprocess import DEVNULL, STDOUT, check_call
import os


def process_ffmpeg(filename, commands, endtime):
    extension = filename.split('.')[-1]
    name_without_ext = ''.join(str(elem) for elem in filename.split('.')[
                               :-1])  # just in case . in name

    def file_path(file, file_ext=extension):
        return f'{get_real_path()}/processing_files/{file}.{file_ext}'

    # IMPORTANT NOTE: the order of commands is processed as non-descructive (commands that preserve timestamps)
    # and then descructive. in this way, we don't have to keep adjusting time stamps. makes it a lot easier.
    # currently:
    # sounds, captions, mutes
    # cuts, timelapses

    # sounds
    for sound_command in commands["music"]:
        cleanup_files = []

        #creates the inital 0 to beginning cut
        cleanup_files.append(file_path(name_without_ext + "_BEFORE"))
        command = f'ffmpeg -i {file_path(name_without_ext)} -to {sound_command["from"]} -c copy {file_path(name_without_ext + "_BEFORE")}'

        if os.system(command) != 0:
            raise Exception('command failed during adding music')


        #creates the middle cut to add music to
        cleanup_files.append(file_path(name_without_ext + "_EDIT_ME"))
        command = f'ffmpeg -i {file_path(name_without_ext)} -ss {sound_command["from"]} -t {sound_command["to"] - sound_command["from"]} -c copy {file_path(name_without_ext + "_EDIT_ME")}'

        if os.system(command) != 0:
            raise Exception('command failed during adding music')

        #creates the cut from to the end
        cleanup_files.append(file_path(name_without_ext + "_AFTER"))
        command = f'ffmpeg -i {file_path(name_without_ext)} -ss {sound_command["to"]} -c copy {file_path(name_without_ext + "_AFTER")}'
        

        if os.system(command) != 0:
            raise Exception('command failed during adding music')

        # do a total overlay of the audio. this is so that we have the "Right" audio
        cleanup_files.append(file_path(name_without_ext + "_EDITED"))
        command = f'ffmpeg -y -stream_loop -1 -i "{get_real_path()}/audio/music/{sound_command["musicType"]}.mp3" -i "{file_path(name_without_ext + "_EDIT_ME")}" -map 0:a:0 -map 1:v:0 -c:v copy -c:a aac -ac 2 -shortest {file_path(name_without_ext + "_EDITED")}'

        if os.system(command) != 0:
            raise Exception('command failed during adding music')

        #take that overlay and lower the volume
        cleanup_files.append(file_path(name_without_ext + "_EDITED_VOLUMED"))
        command = f'ffmpeg -i {file_path(name_without_ext + "_EDITED")} -filter:a "volume=0.07" {file_path(name_without_ext + "_EDITED_VOLUMED")}'

        if os.system(command) != 0:
            raise Exception('command failed during adding music')

        #overlap
        cleanup_files.append(file_path(name_without_ext + "_MIDDLE_STITCH"))
        command = f'ffmpeg -i {file_path(name_without_ext + "_EDITED_VOLUMED")} -i {file_path(name_without_ext + "_EDIT_ME")} -filter_complex "[0:a][1:a]amerge,pan=stereo|c0<c0+c2|c1<c1+c3[a]" -map 1:v -map "[a]" -c:v copy -c:a aac -shortest {file_path(name_without_ext + "_MIDDLE_STITCH")}'

        if os.system(command) != 0:
            raise Exception('command failed during adding music')

        #stitch together the 3 files
        cleanup_files.append(file_path("cuts", "txt"))
        with open(file_path("cuts", "txt"), 'x') as file:
                #0 2 1 because we appended the overlap last
                file.write(f'file {file_path(name_without_ext + "_BEFORE")}\n')
                file.write(f'file {file_path(name_without_ext + "_MIDDLE_STITCH")}\n')
                file.write(f'file {file_path(name_without_ext + "_AFTER")}\n')

        new_name = f'{name_without_ext}_v'
        command = f'ffmpeg -safe 0 -f concat -segment_time_metadata 1 -i {file_path("cuts", "txt")} -vf select=concatdec_select -af aselect=concatdec_select,aresample=async=1 {file_path(new_name)}'

        if os.system(command) != 0:
            raise Exception(f"Command failed at music: \n {command}")

        #cleanup
        for cleanup_file in cleanup_files:
            delete_local_file(cleanup_file)
        delete_local_file(file_path(name_without_ext))
        name_without_ext = new_name


    # captions
    for caption_command in commands["captions"]:
        new_name = f"{name_without_ext}_CAP"

        # draw caption.
        command = f'ffmpeg -i {file_path(name_without_ext)} -vf drawtext="enable=\'between(t,{caption_command["from"]},{caption_command["to"]})\': fontfile={f"{get_real_path()}/worker_process/assets/AvenirNext.otf"}: text=\'{caption_command["text"]}\': fontcolor={caption_command["color"]}: fontsize=24: box=1: boxcolor={caption_command["backgroundColor"]}:boxborderw=5: x=(w-text_w)/2: y=h-th-10:" -codec:a copy {file_path(new_name)}'

        if os.system(command) != 0:
            raise Exception(f"Command failed at caption: \n {command}")

        delete_local_file(file_path(name_without_ext))
        name_without_ext = new_name

    # mutes
    if len(commands["mute"]) > 0:
        #mutes can easily be done all in one command.
        command = ""

        for mute_command in commands["mute"]:
            command += gen_mute(mute_command["from"], mute_command["to"])

        new_name = f"{name_without_ext}_m"
        command = f'ffmpeg -i {file_path(name_without_ext)} -af "{command[:-1]}" {file_path(new_name)}'
        if os.system(command) != 0:
            Exception(f"Command failed at cut: \n {command}")
        delete_local_file(file_path(name_without_ext))
        name_without_ext = new_name

    # cuts
    if len(commands["cuts"]) > 0:
        # inverse ranges so that we instead have the parts we want to KEEP
        cuts = inverse_ranges(
            order_commands(commands["cuts"]), endtime)

        # create many tiny files and store the name.
        files_created_names = []
        for i, command in enumerate(cuts):
            files_created_names.append(f'{name_without_ext}_{i}')
            command = f'ffmpeg -i {file_path(name_without_ext)} -ss {command["from"]} -t {command["to"] - command["from"]} -c copy {file_path(files_created_names[i])}'
            if os.system(command) != 0:
                raise Exception(f"Command failed at cut: \n {command}")

        # create a .txt file with all the filenames in it
        with open(file_path("cuts", "txt"), 'x') as file:
            for cut_file_name in files_created_names:
                file.write(f'file {file_path(cut_file_name)}\n')

        # executes the final command
        new_name = f'{name_without_ext}_c'
        command = f'ffmpeg -f concat -safe 0 -i {file_path("cuts", "txt")} -c copy {file_path(new_name)}'

        if os.system(command) != 0:
            raise Exception(f"Command failed at cut: \n {command}")

        # cleanup
        for cut_file_name in files_created_names:
            delete_local_file(file_path(cut_file_name))
        delete_local_file(file_path('cuts', 'txt'))
        delete_local_file(file_path(name_without_ext))
        name_without_ext = new_name

    #fast forward

    #this is currently the only command that gets executed after cuts. (both destructive)
    #therefore we have to adjust each timelapse to the cut.
    timelapse_adjusted_times = adjust_to_cuts(
        commands["cuts"], order_commands(commands["timelapses"]))
    for timelapse_command in timelapse_adjusted_times:
        new_name = f'{name_without_ext}_ff'
        command = gen_single_ff(timelapse_command["from"], timelapse_command["to"], file_path(
            name_without_ext), file_path(new_name))
        
        if os.system(command) != 0:
            Exception(f"Command failed at lapse: \n {command}")
        name_without_ext = new_name
    return file_path(name_without_ext)
