from helpers import post, upload_to_gcs, delete_from_gcs, download_from_gcs, delete_local_file, get_real_path
from worker_process.worker_process import process_file
from json import dumps, loads
from run import q
import uuid
import os

async def wit_call(blob):
    return await post(url='https://api.wit.ai/speech', data=blob, headers={
        'Content-Type': 'audio/mpeg3',
        'Authorization': f'Bearer {os.environ["WIT_CLIENT_TOKEN"]}'
    })
    
def inital_upload(file, email, commands, endtime):
    generated_uuid = uuid.uuid4()
    filename = f'{generated_uuid}.{file.filename.split(".")[-1]}'
    upload_to_gcs('raw', file.file, filename)
    json = dumps({"filename": filename, "email": email, "commands": loads(commands), "endtime": endtime})
    q.enqueue(process_file, json)

#need to do it this way because fileREsposne doesn't work unless it's local...
#file issue?
def download_to_serve(file_name):
    download_from_gcs('processed', file_name, f'{get_real_path()}/serving_files')
    return f'{get_real_path()}/serving_files/{file_name}'

def cleanup_served(file_name):
    delete_from_gcs('processed', file_name)
    delete_local_file(f'{get_real_path()}/serving_files/{file_name}')
