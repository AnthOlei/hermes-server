from fastapi import FastAPI, Request, Form, File, UploadFile, HTTPException, BackgroundTasks
from fastapi.responses import JSONResponse, FileResponse, StreamingResponse
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from services import wit_call, inital_upload, process_file, download_to_serve, cleanup_served
import uuid
import os
import asyncio
from run import app


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/static", StaticFiles(directory="build/static"), name="static")
app.mount("/audio", StaticFiles(directory="audio"), name="audio")


@app.get("/")
async def hermes():
    return FileResponse('build/index.html')

@app.get("/ping")
def ping():
    return "pong"


@app.get("/favicon.png")
async def favicon():
    return FileResponse('build/favicon.png')


@app.post("/wit/")
async def wit(request: Request):
    return JSONResponse(await wit_call(await request.body()))


@app.post("/submit/")
async def add_video(email: str = Form(...), commands: str = Form(...), file: UploadFile = File(...), endtime: str = Form(...)):
    #NOTE: no server side validation on this is necessary.
    #Nginx will disallow all uploads greater than 0.5GB.
    inital_upload(file, email, commands, endtime)
    return

@app.get('/download/{file_name}')
async def download(file_name: str, background_tasks: BackgroundTasks):
    try:
        response =  FileResponse(download_to_serve(file_name))
        background_tasks.add_task(cleanup_served, file_name=file_name)
        return response
    except:
        await asyncio.sleep(3) #RIP brute force
        raise HTTPException(status_code=404, detail='file not found.')